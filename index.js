/*
    Creator: rater193
    Creation date: 8/13/2019
    Description:
        This is a simple web server, designed to get rid of the traditional vulnterabilities that php gives, while giving the power
        of NodeJS in replacement of PHP, so that way you can execute server side code from the html files, without it having to be
        directly returned to the end user.
*/

//Libraries
const http = require('http');
const fs = require('fs');
const url = require("url");
const vm = require("vm");

//Config
const config = {
    ["StartBlock"]: "<NJS>",
    ["EndBlock"]: "</NJS>",
    ["Port"]: 8080,
    ["ServerDIR"]: "./website/"
}


//Here we are handling the data that we loaded fro mt he file
function handleData(data) {

    //This is used to determine if we are trying to execute code from the code blocks
    let codeStart = false;

    //Here we areconfiguring the starting and ending elements
    let elemStart = config.StartBlock;
    let elemEnd = config.EndBlock;

    //The output of the html file
    let ret = "";

    //The otuput of the generated JS
    let js = "";

    for(let i = 0; i < data.length; i++) {
        let tarChar = elemStart.charAt(0);
        if(codeStart==false) {
            if(data.charAt(i)==tarChar) {
                //Here we are checking if we have the starting code block
                let hasPassed = true;
                for(let __i = 0; __i < elemStart.length; __i++) {
                    if(elemStart.charAt(__i)==data.charAt(i+__i)) {
                    }else{
                        ret += data.charAt(i);
                        hasPassed = false;
                        break;
                    }
                }

                //Here we are passing if the code has passed checking for the custom element
                //that we are wanting to target to generate a string for the code, then
                //execute it on the server
                if(hasPassed) {
                    i += elemStart.length;
                    codeStart = true;
                }
            }else{
                ret += data.charAt(i);
            }
        }else{
            if(data.charAt(i)==tarChar) {
                //Here we are checking if we have the starting code block
                let hasPassed = true;
                for(let __i = 0; __i < elemEnd.length; __i++) {
                    if(elemEnd.charAt(__i)==data.charAt(i+__i)) {
                    }else{
                        js += data.charAt(i);
                        hasPassed = false;
                        break;
                    }
                }

                //Here we are passing if the code has passed checking for the custom element
                //that we are wanting to target to generate a string for the code, then
                //execute it on the server
                if(hasPassed) {
                    i += elemEnd.length;
                    let t = new Function(js);
                    let output = t();
                    //let output = t();
                    //script = new vm.Script('const output = t();');
                    //script.runInNewContext({["t"]:t});
                    //ret+=vm.output!=null?vm.output:"";
                    ret+=output!=null?output:"";
                    js = "";
                    codeStart = false;
                }
            }else{
                js += data.charAt(i);
            }
        }
    }

    return ret;
}

//This is used to get the extension of a file
function getFileExtension(filename) {
    let canbuild = false;
    let ret = "";

    for(let i = 0; i < filename.length; i++) {
        if(filename.charAt(i)==".") {
            canbuild = true;
            ret = "";
        }else{
            if(canbuild) {
                ret += filename.charAt(i);
            }
        }
    }

    return ret;
}


//Here we are sending the raw image
function sendImage(_t, req, res, filename) {
    
    res.writeHead(200, {'Content-Type': _t});
    if(fs.existsSync(filename)) {
        let index = fs.readFileSync(filename);
        res.write(index);
    }
    res.end();
}

//Here we are sending the html file
function sendHTML(req, res, filename) {
    //Write the message header
    res.writeHead(200, {'Content-Type': 'text/html'});
    if(fs.existsSync(filename)) {
        let index = fs.readFileSync(filename).toString();
        //Here wea re parsing the returned string, to try to see if we can execute custom NJS data
        let data = handleData(index);
        res.write(data);
    }
    res.end();
}

//Here we are starting the HTML server
http.createServer(function (req, res) {
    //Here we are getting the header of the request

    //Here we are getting the default index.html, or returning the header
    let header = url.parse(req.url, true).pathname;
    let filename = header=="/"?"./" + config.ServerDIR + "/index.html":"./" + config.ServerDIR + "/" + header;
    let ext = getFileExtension(filename);
    //console.log("ext: " + ext);

    switch(ext) {
        case "html":
            sendHTML(req, res, filename);
        break;

        case "ico":
            sendImage("image/ico", req, res, filename);
        break;

        case "jpg":
            sendImage("image/jpeg", req, res, filename);
        break;

        case "gif":
            sendImage("image/gif", req, res, filename);
        break;

        case "png":
            sendImage("image/png", req, res, filename);
        break;

        case "svg":
            sendImage("image/svg+xml", req, res, filename);
        break;

        case "js":
            //text/plain
            res.writeHead(200, {'Content-Type': 'application/javascript'});
            if(fs.existsSync(filename)) {
                let index = fs.readFileSync(filename).toString();
                res.write(index);
            }
            res.end();
        break;

        case "css":
            //text/plain
            res.writeHead(200, {'Content-Type': 'text/css'});
            if(fs.existsSync(filename)) {
                let index = fs.readFileSync(filename).toString();
                res.write(index);
            }
            res.end();
        break;

        case "htm":
            sendHTML(req, res, filename);
        break;

        case "njs":
            sendHTML(req, res, filename);
        break;

        default:
            //text/plain
            res.writeHead(200, {'Content-Type': 'text/plain'});
            if(fs.existsSync(filename)) {
                let index = fs.readFileSync(filename).toString();
                res.write(index);
            }
            res.end();
            console.log("Unhandled extension: " + console.log("ext: " + ext));
        break;
    } 


}).listen(config.Port);
